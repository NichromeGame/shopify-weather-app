import axios from "./axios";
import { CancelToken } from "axios";

export type ForecastWeatherRequest = any;

export const query = (name: string, cancelToken?: CancelToken) =>
  axios<any>({
    url: `/data/2.5/weather?q=${name}&appid=1e50609203ee36bc2c7ee1f2b8678db4`,
    method: "GET",
    cancelToken,
  });
