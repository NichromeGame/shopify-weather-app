import React, {useState, useCallback, useEffect} from 'react';
import {
  Layout,
  Page,
  FooterHelp,
  Card,
  Link,
  Button,
  FormLayout,
  TextField,
  AccountConnection,
  ChoiceList,
  SettingToggle,
} from '@shopify/polaris';
import {ImportMinor} from '@shopify/polaris-icons';
import { Favorites } from "./redux/cities";
import axios, { CancelTokenSource } from "axios";
import { query } from "./services/whether";
import { List } from '@shopify/polaris';
import { add, removeById, clearAll } from './redux/cities'
import {
  Route,
  BrowserRouter as Router,
  Redirect,
  Switch,
} from "react-router-dom";
import { Home } from './pages/Home';
import { FavoritesComponent } from './pages/Favorites';

interface AccountProps {
  onAction(): void;
}

let cancelToken: CancelTokenSource | undefined;

export default function App() {
  const [searchValue, setSearchValue] = useState("");
  const [cities, setCities] = useState<Favorites[]>([]);
  const [loading, setLoading] = useState(false);

  console.log('cities', cities)

  const fetch = async (value: string) => {
    try {
      cancelToken && cancelToken.cancel();
      cancelToken = axios.CancelToken.source();

      setLoading(true);
      const data = await query(value, cancelToken.token);
      setLoading(false);

      setCities([data])
    } catch (error) {
      console.log(error);
      setCities([])
      setLoading(false);
    }
  };

  const onChange = (value: string) => {
    setSearchValue(value);
    if (!value) return;
    fetch(value)
  };

  const onClick = () =>  {setCities([]); add(cities[0])}

  useEffect(() => {
    fetch('Moscow');
  }, [])
  const [connected, setConnected] = useState(false);


  const toggleConnection = useCallback(() => {
    setConnected(!connected);
  }, [connected]);

  const accountMarkup = connected ? (
    <DisconnectAccount onAction={toggleConnection} />
  ) : (
    <ConnectAccount onAction={toggleConnection} />
  );

  return (
    <Page
      title="Polaris"
    >
    <Router>
      <Switch>
        <Route exact component={Home} path="/" />
        <Route exact component={FavoritesComponent} path="/favorites" />
        <Redirect to={"/"} />
      </Switch>
    </Router>
    </Page>
  );
}

function ConnectAccount({onAction}: AccountProps) {
  return (
    <AccountConnection
      action={{content: 'Connect', onAction}}
      details="No account connected"
      termsOfService={
        <p>
          By clicking Connect, you are accepting Sample’s{' '}
          <Link url="https://polaris.shopify.com">Terms and Conditions</Link>,
          including a commission rate of 15% on sales.
        </p>
      }
    />
  );
}

function DisconnectAccount({onAction}: AccountProps) {
  return (
    <AccountConnection
      connected
      action={{content: 'Disconnect', onAction}}
      accountName="Tom Ford"
      title={<Link url="http://google.com">Tom Ford</Link>}
      details="Account id: d587647ae4"
    />
  );
}
