import { configureStore } from '@reduxjs/toolkit';
import reducer from './cities';

export default configureStore({
  reducer: {
    favorites: reducer,
  },
});
