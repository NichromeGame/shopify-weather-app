import { createSlice } from '@reduxjs/toolkit';
import { getItemFromStorage, placeItemToStorage } from '../constants.ts/storage';

const storageLimit = 10

export type Favorites = any;

export const favoritesSlice = createSlice({
  name: 'favorites',
  initialState: {
    value: getItemFromStorage() as Favorites[],
  },
  reducers: {
    add: (state, action) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      const value = state.value
      const oldValue = value.length < 10 ? value : value.filter((_, i) => i !== 0);
      const newValue = [...oldValue.filter(i => i.name !== action.payload.name), action.payload as Favorites];
      placeItemToStorage(newValue);
      state.value = newValue;
    },
    removeById: (state, action) => {
        // Redux Toolkit allows us to write "mutating" logic in reducers. It
        // doesn't actually mutate the state because it uses the Immer library,
        // which detects changes to a "draft state" and produces a brand new
        // immutable state based off those changes
        const newValue = state.value.filter(({name}) => name !== action.payload as string);
        placeItemToStorage(newValue);
        state.value = newValue;
      },
    clearAll: (state) => {
      state.value = [];
    },
  },
});

export const { add, removeById, clearAll } = favoritesSlice.actions;

// // The function below is called a thunk and allows us to perform async logic. It
// // can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// // will call the thunk with the `dispatch` function as the first argument. Async
// // code can then be executed and other actions can be dispatched
// export const incrementAsync = amount => dispatch => {
//   setTimeout(() => {
//     dispatch(incrementByAmount(amount));
//   }, 1000);
// };

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectFavorites = (state: any) => state.favorites.value;

export default favoritesSlice.reducer;
