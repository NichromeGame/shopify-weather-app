export const cityStorageName = 'cities';

// * any - because value of storage can be any
export const placeItemToStorage = (value: any, key = cityStorageName) =>
    localStorage.setItem(key, JSON.stringify(value));
export const getItemFromStorage = (
    key = cityStorageName,
    type: 'string' | 'array' | 'object' = 'array'
) => {
    const user = localStorage.getItem(key);

    const str = type === 'string' ? '' : type === 'array' ? '[]' : '{}'

    return JSON.parse(user || str);
};
export const removeItemFromStorage = (key = cityStorageName) => localStorage.removeItem(key);
